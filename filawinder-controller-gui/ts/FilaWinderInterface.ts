import { GUI, Controller } from "./GUI"

export interface FilaWinderInterface {
	initializedCommunication: boolean
	servoLeftChanged(send: boolean): void
	servoRightChanged(send: boolean): void
	servoCurrentChanged(send: boolean): void
	speedChanged(send: boolean): void
}