/// <reference path="../node_modules/@types/three/index.d.ts"/>
/// <reference path="../node_modules/@types/jquery/index.d.ts"/>
/// <reference path="../node_modules/@types/paper/index.d.ts"/>
/// <reference path="../node_modules/@types/file-saver/index.d.ts"/>

// import Stats = require("../node_modules/three/examples/js/libs/stats.min.js")
// import { Stats } from "../node_modules/three/examples/js/libs/stats.min.js"
// import { THREE } from "../node_modules/three/build/three"

import { Settings, settingsManager } from "./Settings"
// import { FilaWinder, filaWinder } from "./FilaWinder"
import { Renderer } from "./Renderer"
// import { Pen } from "./Pen"
// import { SVGPlot } from "./Plot"
import { Communication } from "./Communication/Communication"
import { CommandDisplay } from "./Communication/CommandDisplay"
import { GUI } from "./GUI"
import { Console } from "./Console"
import { FilaWinder, filaWinder } from "./FilaWinder"

declare var addWheelListener: any
declare var dat: any

let communication: Communication = null

let container = null

let renderer: Renderer = null

let gui: GUI

let positionPreview: paper.Path = null

let drawing = {
	scale: 1,
}

let w = <any>window

document.addEventListener("DOMContentLoaded", function(event) { 


	function initialize() {

		dat.GUI.DEFAULT_WIDTH = 325
		gui = new GUI({ autoPlace: false })

		let controllerConsole = new Console()
		let commandDisplay = new CommandDisplay()
		commandDisplay.createGUI(controllerConsole.gui)
		controllerConsole.createGUI()

		let customContainer = document.getElementById('gui')
		customContainer.appendChild(gui.getDomElement())
		
		communication = new Communication(gui)
		communication.setFilaWinder(filaWinder)

		let commandFolder = gui.addFolder('Commands')
		commandFolder.open()

		renderer = new Renderer()
		
		filaWinder.createGUI(commandFolder)
		filaWinder.initialize()

		settingsManager.createGUI(gui)
		settingsManager.setFilaWinder(filaWinder)

		

		// debug

		w.settingsManager = settingsManager
		w.gui = gui
		w.renderer = renderer
		w.communication = communication
		w.commandDisplay = commandDisplay
	}

	initialize()
	
	let animate = () => {
		w.nCall = 0
		requestAnimationFrame( animate )
		renderer.render()
	}

	animate()

	function windowResize() {
		renderer.windowResize()
	}

	function eventWasOnGUI(event: MouseEvent) {
		return $.contains(gui.getDomElement(), <any>event.target)
	}

	function mouseDown(event: MouseEvent) {
		renderer.mouseDown(event)
	}

	function mouseMove(event: MouseEvent) {
		renderer.mouseMove(event)
	}

	function mouseUp(event: MouseEvent) {
		renderer.mouseUp(event)
	}

	function mouseLeave(event: MouseEvent) {
		renderer.mouseLeave(event)
	}

	function mouseWheel(event: WheelEvent) {
		renderer.mouseWheel(event)
	}

	function keyDown(event: KeyboardEvent) {
		filaWinder.keyDown(event)
		renderer.keyDown(event)
	}

	function keyUp(event: KeyboardEvent) {
		filaWinder.keyUp(event)
		renderer.keyUp(event)
	}

	window.addEventListener( 'resize', windowResize, false )
	document.body.addEventListener('mousedown', mouseDown)
	document.body.addEventListener('mousemove', mouseMove)
	document.body.addEventListener('mouseup', mouseUp)
	document.body.addEventListener('mouseleave', mouseLeave)
	document.body.addEventListener('keydown', keyDown)
	document.body.addEventListener('keyup', keyUp)
	addWheelListener(document.body, mouseWheel)
});
