import { GUI, Controller } from "./GUI"
import { FilaWinderInterface } from "./FilaWinderInterface"


export let Settings = {
	autoConnect: true,
	firmware: 'FilaWinder',
	speed: 10,
	guide: {
		left: 20,
		right: 160,
		current: 90
	}
}

const MAX_SPEED = 10000

declare let saveAs: any

export class SettingsManager {
	
	gui: GUI = null
	filaWinder: FilaWinderInterface

	constructor() {
		this.loadLocalStorage()
	}

	setFilaWinder(filaWinder: FilaWinderInterface) {
		this.filaWinder = filaWinder
	}

	getControllers() {
		return this.gui.getFolder('Settings').getAllControllers()
	}

	createGUI(gui: GUI) {
		this.gui = gui
		let settingsFolder = gui.addFolder('Settings')
		settingsFolder.open()

		let loadSaveFolder = settingsFolder.addFolder('Load & Save')
		loadSaveFolder.addFileSelectorButton('Load', 'application/json', (event:any) => this.handleFileSelect(event) )
		loadSaveFolder.add(this, 'save').name('Save')

		let guideFolder = settingsFolder.addFolder('Guide')
		guideFolder.add(Settings.guide, 'left', 0, 180, 1).name('Left angle')
		guideFolder.add(Settings.guide, 'right', 0, 180, 1).name('Right angle')
		guideFolder.add(Settings.guide, 'current', 0, 180, 1).name('Current angle')
		guideFolder.open()
		
		settingsFolder.add(Settings, 'speed', 0, 200, 1).name('Speed in rpm')

		let controllers = this.getControllers()

		for(let controller of controllers) {
			let name = controller.getName()
			let parentNames = controller.getParentNames()
			controller.onChange( (value: any) => this.settingChanged(parentNames, name, value, false) )
			controller.onFinishChange( (value: any) => this.settingChanged(parentNames, name, value, true) )
		}
	}

	settingChanged(parentNames: string[], name: string, value: any=null, changeFinished=false) {

		// update sliders and transmit change to concerned object
		if(parentNames[0] == 'Guide') {
			if(name == 'left') {
				this.filaWinder.servoLeftChanged(changeFinished)
			} else if(name == 'right') {
				this.filaWinder.servoRightChanged(changeFinished)
			} else if(name == 'current') {
				this.filaWinder.servoCurrentChanged(changeFinished)
			}
		}
		if(name == 'speed') {
			this.filaWinder.speedChanged(changeFinished)
		}
		document.dispatchEvent(new CustomEvent('SettingChanged', { detail: { parentNames: parentNames, name: name, value: value, changeFinished: changeFinished } }))
		this.save(false)
	}

	// When loading settings (load from json file)
	settingsChanged() {

		for(let controller of this.getControllers()) {
			controller.updateDisplay()
		}

		document.dispatchEvent(new CustomEvent('SettingChanged', { detail: { all: true } }))

		// save to local storage
		this.save(false)
	}

	save(saveFile=true) {
		let json = JSON.stringify(Settings, null, '\t')
		localStorage.setItem('settings', json)
		if(saveFile) {
			var blob = new Blob([json], {type: "application/json"})
			saveAs(blob, "settings.json")
		}
	}

	updateSliders() {
		let controllers = this.getControllers()

		for(let controller of controllers) {
			controller.updateDisplay()
		}
	}

	copyObjectProperties(target: any, source: any) {
		if(source == null) {
			return
		}
		for(let property in target) {
			if(typeof(target[property]) === 'object') {
				this.copyObjectProperties(target[property], source[property])
			} else if (source[property] != null) {
				if(typeof target[property] == typeof source[property]) {
					target[property] = source[property]
				}
			}
		}
	}

	copyObjectPropertiesFromJSON(target: any, jsonSource: any) {
		if(jsonSource == null) {
			return
		}
		this.copyObjectProperties(target, JSON.parse(jsonSource))
	}

	onJsonLoad(event: any) {
		if(event.target != null && event.target.result != null) {
			this.copyObjectPropertiesFromJSON(Settings, event.target.result)
			this.settingsChanged()
			this.updateSliders()
		}
	}

	handleFileSelect(event: any) {
		let files: FileList = event.dataTransfer != null ? event.dataTransfer.files : event.target.files

		for (let i = 0; i < files.length; i++) {
			let file = files.item(i)
			
			let reader = new FileReader()
			reader.onload = (event:any) => this.onJsonLoad(event)
			reader.readAsText(file)
		}
	}

	loadLocalStorage() {
		this.copyObjectPropertiesFromJSON(Settings, localStorage.getItem('settings'))
	}
}

export let settingsManager = new SettingsManager()