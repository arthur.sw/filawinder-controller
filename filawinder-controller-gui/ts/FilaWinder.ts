import { Communication, communication } from "./Communication/Communication"
import { Settings, SettingsManager, settingsManager } from "./Settings"
import { GUI, Controller } from "./GUI"
import { FilaWinderInterface } from "./FilaWinderInterface"

export class FilaWinder implements FilaWinderInterface {

	gui:GUI = null
	initializedCommunication = false
	
	guide: paper.Group
	guideLeft: paper.Path
	guideRight: paper.Path
	guideCurrent: paper.Path
	previousSpeed: number = 0

	constructor() {
	}

	checkInitialized() {
		if(!this.initializedCommunication) {
			communication.interpreter.initialize()
		}
	}
	
	createGUI(gui: GUI) {
		this.gui = gui
		// let position = { moveX: Settings.filaWinder.homeX, moveY: Settings.filaWinder.homeY }
		// gui.add(position, 'moveX', 0, Settings.filaWinder.width).name('Move X').onFinishChange((value)=> this.setX(value))
		// gui.add(position, 'moveY', 0, Settings.filaWinder.height).name('Move Y').onFinishChange((value)=> this.setY(value))

		gui.addButton('Calibrate', ()=> this.calibrate())
		gui.addButton('Start', ()=> this.start())
		gui.addButton('Stop', ()=> this.stop())
		gui.addButton('Constant speed', ()=> this.constantSpeed())


		// this.penStateButton = gui.addButton('Pen down', () => this.togglePenState() )
		// this.motorsEnableButton = gui.addButton('Disable motors', ()=> this.toggleMotors())
		
		// // DEBUG
		// gui.addButton('Send specs', ()=> communication.interpreter.initialize(false))
	}

	initialize() {
		let background = paper.Path.Rectangle(paper.project.view.bounds)
		background.fillColor = 'white'
		background.strokeColor = null

		this.guide = new paper.Group()

		this.guideLeft = new paper.Path()
		this.guideRight = new paper.Path()
		this.guideCurrent = new paper.Path()

		this.guideLeft.applyMatrix = false
		this.guideRight.applyMatrix = false
		this.guideCurrent.applyMatrix = false

		this.guideLeft.strokeWidth = 1
		this.guideRight.strokeWidth = 1

		let pivot = new paper.Point(0, 0)

		this.guideLeft.add(new paper.Point(0, 0))
		this.guideLeft.add(new paper.Point(200, 0))
		this.guideLeft.pivot = pivot
		this.guideLeft.rotation = 0
		this.guide.addChild(this.guideLeft)

		this.guideRight.add(new paper.Point(0, 0))
		this.guideRight.add(new paper.Point(200, 0))
		this.guideRight.pivot = pivot
		this.guideRight.rotation = 0
		this.guide.addChild(this.guideRight)

		this.guideCurrent.add(new paper.Point(0, 0))
		this.guideCurrent.add(new paper.Point(200, 0))
		this.guideCurrent.strokeColor = 'blue'
		this.guideCurrent.strokeWidth = 2
		this.guideCurrent.pivot = pivot
		this.guideCurrent.rotation = 0
		this.guide.addChild(this.guideCurrent)
		paper.settings.hitTolerance = 20

		paper.project.activeLayer.onMouseMove = (event: any)=> {
			let hitResult = paper.project.hitTest(event.point)
			if(hitResult.item.parent == this.guide) {
				$('#canvas').addClass('pointer')
			} else {
				$('#canvas').removeClass('pointer')
			}
		}

		// paper.project.activeLayer.onMouseUp = (event: any)=> {
		// 	let hitResult = paper.project.hitTest(event.point)
		// 	if(hitResult.item == this.guideLeft) {
		// 		$('#canvas').addClass('pointer')
		// 	} else {
		// 		$('#canvas').removeClass('pointer')
		// 	}
		// }

		this.guide.position = paper.project.view.bounds.center.add(this.guide.bounds.size.multiply(0.5))

		this.servoLeftChanged(false)
		this.servoRightChanged(false)
		this.servoCurrentChanged(false)
	}

	start() {
		communication.interpreter.sendStart()
	}

	stop() {
		communication.interpreter.sendReset()
	}

	calibrate() {
		communication.interpreter.sendCalibrate()
	}

	constantSpeed() {
		communication.interpreter.sendReset()
		communication.interpreter.sendConstantSpeed()
	}

	servoLeftChanged(sendChange: boolean) {
		if(sendChange) {
			communication.interpreter.sendServoLeft()
		}
		this.guideLeft.rotation = -180+Settings.guide.left
	}
	servoRightChanged(sendChange: boolean) {
		if(sendChange) {
			communication.interpreter.sendServoRight()
		}
		this.guideRight.rotation = -180+Settings.guide.right
	}
	servoCurrentChanged(sendChange: boolean) {
		if(sendChange) {
			communication.interpreter.sendServoCurrent()
		}
		this.guideCurrent.rotation = -180+Settings.guide.current
	}
	speedChanged(sendChange: boolean) {
		if(sendChange && this.previousSpeed != Settings.speed) {
			communication.interpreter.sendReset()
			communication.interpreter.sendSpeed()
			communication.interpreter.sendConstantSpeed()
			this.previousSpeed = Settings.speed
		}
	}


	keyDown(event:KeyboardEvent) {
		let amount = event.shiftKey ? 25 : event.ctrlKey ? 10 : event.altKey ? 5 : 1
		switch (event.keyCode) {
			case 37: 			// left arrow
				// this.moveDirect(this.getPosition().add(new paper.Point(-amount, 0)))
				break;
			case 38: 			// up arrow
				// this.moveDirect(this.getPosition().add(new paper.Point(0, -amount)))
				break;
			case 39: 			// right arrow
				// this.moveDirect(this.getPosition().add(new paper.Point(amount, 0)))
				break;
			case 40: 			// down arrow
				// this.moveDirect(this.getPosition().add(new paper.Point(0, amount)))
				break;
			
			default:
				break;
		}
	}

	keyUp(event:KeyboardEvent) {
	}

	windowResize() {
	}
}

export let filaWinder: FilaWinder = new FilaWinder()