import { Settings, SettingsManager } from "../Settings"
import { FilaWinderInterface } from "../FilaWinderInterface"

const MAX_INPUT_BUFFER_LENGTH = 500

export declare type Command = {
	data: string
	message: string
	callback: ()=> any
	id: number
}

export declare type Communication = { send: (type: string, data?: any)=> void }

export class Interpreter {
	
	serialPort: string
	communication: Communication
	commandID = 0
	commandQueue: Array<Command>
	filaWinder: FilaWinderInterface
	pause: boolean
	tempoNextCommand: boolean
	serialInput: string
	readonly continueMessage = 'READY'
	serialCommunicationSpeed = 115200

	constructor(communication: Communication) {
		this.commandQueue = []
		this.pause = false
		this.serialInput = ''
		this.tempoNextCommand = false
		this.communication = communication
	}

	setSerialPort(serialPort: string) {
		this.serialPort = serialPort;
	}

	setFilaWinder(filaWinder: FilaWinderInterface) {
		this.filaWinder = filaWinder;
	}

	serialPortConnectionOpened() {
		this.initialize()
	}

	initialize(initializeAtHome=true) {
		this.filaWinder.initializedCommunication = true
	}

	send(command: Command) {
		if(this.pause) {
			return
		}
		document.dispatchEvent(new CustomEvent('SendCommand', { detail: command }))
		console.log('send: ' + command.message + ' - ' + command.data)
		this.communication.send('data', command.data)
	}

	messageReceived(message: string) {
		if(message == null) {
			return
		}

		this.serialInput += message

		let messages = this.serialInput.split('\n')
		
		// process all messages except the last one (it is either empty if the serial input ends with '\n', or it is not a finished message)
		for(let i=0 ; i<messages.length-1 ; i++) {
			this.processMessage(messages[i])
		}

		// Clear any old message
		if(this.serialInput.endsWith('\n')) {
			this.serialInput = ''
		} else {
			this.serialInput = messages[messages.length-1]
		}
	}

	processMessage(message: string) {
		if(message.indexOf('-p: l: ') != 0) {
			console.log(message)
		}
		
		// if(message.indexOf('++')==0) {
		// 	console.log(message)
		// }
		
		document.dispatchEvent(new CustomEvent('MessageReceived', { detail: message }))

		if(message.indexOf(this.continueMessage) == 0) {
			if(this.commandQueue.length > 0) {
				let command = this.commandQueue.shift()
				if(command.callback != null) {
					command.callback()
				}
				document.dispatchEvent(new CustomEvent('CommandExecuted', { detail: command }))
				if(this.commandQueue.length > 0) {
					this.send(this.commandQueue[0])
				} else {
					this.queueEmpty()
				}
			}
		}
	}

	queueEmpty() {

	}

	setPause(pause: boolean) {
		this.pause = pause;
		if(!this.pause && this.commandQueue.length > 0) {
			this.send(this.commandQueue[0])
		}
	}

	queue(data: string, message: string, callback: () => any = null) {
		let command = { id: this.commandID++, data: data, callback: callback, message: message }
		document.dispatchEvent(new CustomEvent('QueueCommand', { detail: command }))

		this.commandQueue.push(command)

		if(this.commandQueue.length == 1) {
			this.send(command)
		}
	}

	removeCommand(commandID: number) {
		let index = this.commandQueue.findIndex((command)=> command.id == commandID)
		if(index >= 0) {
			this.commandQueue.splice(index, 1)
		}
	}

	clearQueue() {
		this.commandQueue = []
		document.dispatchEvent(new CustomEvent('ClearQueue', { detail: null }))
	}

	sendStart(callback: ()=> void = null) {
		let message = 'Start'
		this.queue('G0\n', message, callback)
	}

	sendStop(callback: ()=> void = null) {
		let message = 'Stop'
		this.queue('G1\n', message, callback)
	}

	sendCalibrate(callback: ()=> void = null) {
		let message = 'Calibrate'
		this.queue('M4\n', message, callback)
	}

	sendConstantSpeed(callback: ()=> void = null) {
		let message = 'Constant speed'
		this.queue('M5\n', message, callback)
	}

	sendSpeed(speed: number = Settings.speed, callback: ()=> void = null) {
		let message = 'Set speed: ' + speed.toFixed(2)
		this.queue('M6 F' + speed.toFixed(2) + '\n', message, callback)
	}

	sendSetup(callback: ()=> void = null) {
		let message = 'Setup'
		this.queue('M10\n', message, callback)
	}

	sendReset(force = true) {
		if(force) {
			this.communication.send('data', 'M0\n')
			return
		}
		let message = 'Stop'
		this.queue('M0\n', message)
	}

	sendServoLeft(value: number = Settings.guide.left, callback: ()=> void = null) {
		let message = 'Set servo left: ' + value.toFixed(2)
		this.queue('M1 X' + value.toFixed(2) + '\n', message, callback)
	}

	sendServoRight(value: number = Settings.guide.right, callback: ()=> void = null) {
		let message = 'Set servo right: ' + value.toFixed(2)
		this.queue('M2 X' + value.toFixed(2) + '\n', message, callback)
	}

	sendServoCurrent(value: number = Settings.guide.current, callback: ()=> void = null) {
		let message = 'Set servo current: ' + value.toFixed(2)
		this.queue('M3 X' + value.toFixed(2) + '\n', message, callback)
	}

}