import { Settings } from "./Settings"

export class Renderer {
	
	canvas: HTMLCanvasElement
	dragging: boolean
	previousPosition: paper.Point
	spacePressed: boolean

	constructor() {
		this.canvas = document.createElement('canvas')
		let containerJ = $('#canvas')
		this.canvas.width = containerJ.width()
		this.canvas.height = containerJ.height()
		containerJ.get(0).appendChild(this.canvas)

		paper.setup(<any>this.canvas)
		paper.project.currentStyle.strokeColor = 'black'
		paper.project.currentStyle.strokeWidth = 0.5
		paper.project.currentStyle.strokeScaling = false

		this.dragging = false
		this.previousPosition = new paper.Point(0, 0)

		document.addEventListener('SettingChanged', (event: CustomEvent)=> this.onSettingChanged(event), false)
	}

	onSettingChanged(event: CustomEvent) {
		// if(event.detail.all || event.detail.parentNames[0] == 'Machine dimensions') {
			// this.centerOnFilaWinder(Settings.filaWinder, true)
		// }
	}

	centerOnFilaWinder(filaWinder: {width: number, height: number}, zoom=true) {
		if(zoom) {
			let margin = 200
			let ratio = Math.max((filaWinder.width + margin) / this.canvas.width * window.devicePixelRatio, (filaWinder.height + margin) / this.canvas.height * window.devicePixelRatio)
			paper.view.zoom = 1 / ratio
		}

		paper.view.setCenter(new paper.Point(filaWinder.width / 2, filaWinder.height / 2))
	}

	getDomElement(): any {
		return paper.view.element
	}

	windowResize(){
		let containerJ = $('#canvas')
		let width = containerJ.width()
		let height = containerJ.height()
		let canvasJ = $(this.canvas)
		canvasJ.width(width)
		canvasJ.height(height)
		paper.view.viewSize = new paper.Size(width, height)
	}

	getMousePosition(event: MouseEvent): paper.Point {
		return new paper.Point(event.clientX, event.clientY)
	}

	getWorldPosition(event: MouseEvent): paper.Point {
		return paper.view.viewToProject(this.getMousePosition(event))
	}

	mouseDown(event: MouseEvent) {
		this.dragging = true
		this.previousPosition = this.getMousePosition(event)
	}

	mouseMove(event: MouseEvent) {
		if(event.buttons == 4 || this.spacePressed && this.dragging) { 											// wheel button
			let position = this.getMousePosition(event)
			paper.view.translate(position.subtract(this.previousPosition).divide(paper.view.zoom))
			paper.view.draw()
			this.previousPosition.x = position.x
			this.previousPosition.y = position.y
		}
	}

	mouseUp(event: MouseEvent) {
		this.dragging = false
	}

	mouseLeave(event: MouseEvent) {
		this.dragging = false
	}

	mouseWheel(event: WheelEvent) {
		if(event.target != this.getDomElement()) {
			return
		}
		let cursorPosition = this.getWorldPosition(event)
		paper.view.zoom = Math.max(0.1, Math.min(5, paper.view.zoom + event.deltaY / 500))
		let newCursorPosition = this.getWorldPosition(event)
		paper.view.translate(newCursorPosition.subtract(cursorPosition))
	}

	keyDown(event: KeyboardEvent) {
		switch (event.keyCode) {
			case 32: 			// space
				this.spacePressed = true
				$('#canvas').addClass('grab')
		}
	}

	keyUp(event: KeyboardEvent) {
		switch (event.keyCode) {
			case 32: 			// space
				this.spacePressed = false
				$('#canvas').removeClass('grab')
		}
	}

	render() {
	}
}