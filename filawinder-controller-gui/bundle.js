/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 8);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.Settings = {
    autoConnect: true,
    firmware: 'FilaWinder',
    speed: 10,
    guide: {
        left: 20,
        right: 160,
        current: 90
    }
};
const MAX_SPEED = 10000;
class SettingsManager {
    constructor() {
        this.gui = null;
        this.loadLocalStorage();
    }
    setFilaWinder(filaWinder) {
        this.filaWinder = filaWinder;
    }
    getControllers() {
        return this.gui.getFolder('Settings').getAllControllers();
    }
    createGUI(gui) {
        this.gui = gui;
        let settingsFolder = gui.addFolder('Settings');
        settingsFolder.open();
        let loadSaveFolder = settingsFolder.addFolder('Load & Save');
        loadSaveFolder.addFileSelectorButton('Load', 'application/json', (event) => this.handleFileSelect(event));
        loadSaveFolder.add(this, 'save').name('Save');
        let guideFolder = settingsFolder.addFolder('Guide');
        guideFolder.add(exports.Settings.guide, 'left', 0, 180, 1).name('Left angle');
        guideFolder.add(exports.Settings.guide, 'right', 0, 180, 1).name('Right angle');
        guideFolder.add(exports.Settings.guide, 'current', 0, 180, 1).name('Current angle');
        guideFolder.open();
        settingsFolder.add(exports.Settings, 'speed', 0, 200, 1).name('Speed in rpm');
        let controllers = this.getControllers();
        for (let controller of controllers) {
            let name = controller.getName();
            let parentNames = controller.getParentNames();
            controller.onChange((value) => this.settingChanged(parentNames, name, value, false));
            controller.onFinishChange((value) => this.settingChanged(parentNames, name, value, true));
        }
    }
    settingChanged(parentNames, name, value = null, changeFinished = false) {
        // update sliders and transmit change to concerned object
        if (parentNames[0] == 'Guide') {
            if (name == 'left') {
                this.filaWinder.servoLeftChanged(changeFinished);
            }
            else if (name == 'right') {
                this.filaWinder.servoRightChanged(changeFinished);
            }
            else if (name == 'current') {
                this.filaWinder.servoCurrentChanged(changeFinished);
            }
        }
        if (name == 'speed') {
            this.filaWinder.speedChanged(changeFinished);
        }
        document.dispatchEvent(new CustomEvent('SettingChanged', { detail: { parentNames: parentNames, name: name, value: value, changeFinished: changeFinished } }));
        this.save(false);
    }
    // When loading settings (load from json file)
    settingsChanged() {
        for (let controller of this.getControllers()) {
            controller.updateDisplay();
        }
        document.dispatchEvent(new CustomEvent('SettingChanged', { detail: { all: true } }));
        // save to local storage
        this.save(false);
    }
    save(saveFile = true) {
        let json = JSON.stringify(exports.Settings, null, '\t');
        localStorage.setItem('settings', json);
        if (saveFile) {
            var blob = new Blob([json], { type: "application/json" });
            saveAs(blob, "settings.json");
        }
    }
    updateSliders() {
        let controllers = this.getControllers();
        for (let controller of controllers) {
            controller.updateDisplay();
        }
    }
    copyObjectProperties(target, source) {
        if (source == null) {
            return;
        }
        for (let property in target) {
            if (typeof (target[property]) === 'object') {
                this.copyObjectProperties(target[property], source[property]);
            }
            else if (source[property] != null) {
                if (typeof target[property] == typeof source[property]) {
                    target[property] = source[property];
                }
            }
        }
    }
    copyObjectPropertiesFromJSON(target, jsonSource) {
        if (jsonSource == null) {
            return;
        }
        this.copyObjectProperties(target, JSON.parse(jsonSource));
    }
    onJsonLoad(event) {
        if (event.target != null && event.target.result != null) {
            this.copyObjectPropertiesFromJSON(exports.Settings, event.target.result);
            this.settingsChanged();
            this.updateSliders();
        }
    }
    handleFileSelect(event) {
        let files = event.dataTransfer != null ? event.dataTransfer.files : event.target.files;
        for (let i = 0; i < files.length; i++) {
            let file = files.item(i);
            let reader = new FileReader();
            reader.onload = (event) => this.onJsonLoad(event);
            reader.readAsText(file);
        }
    }
    loadLocalStorage() {
        this.copyObjectPropertiesFromJSON(exports.Settings, localStorage.getItem('settings'));
    }
}
exports.SettingsManager = SettingsManager;
exports.settingsManager = new SettingsManager();


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const Settings_1 = __webpack_require__(0);
const Interpreter_1 = __webpack_require__(7);
// export const 57600 = 57600
exports.SERIAL_COMMUNICATION_SPEED = 115200;
let PORT = window.localStorage.getItem('port') || 6842;
class Communication {
    constructor(gui) {
        this.autoConnectIntervalID = -1;
        this.serialPortConnectionOpened = false;
        exports.communication = this;
        this.socket = null;
        this.createGUI(gui);
        this.portController = null;
        this.initializeInterpreter(Settings_1.Settings.firmware);
        this.connectToSerial();
        if (Settings_1.Settings.autoConnect) {
            this.startAutoConnection();
        }
    }
    createGUI(gui) {
        this.gui = gui.addFolder('Communication');
        this.folderTitle = $(this.gui.getDomElement()).find('.title');
        this.folderTitle.append($('<icon>').addClass('serial').append(String.fromCharCode(9679)));
        this.folderTitle.append($('<icon>').addClass('websocket').append(String.fromCharCode(9679)));
    }
    setFilaWinder(filaWinder) {
        this.interpreter.setFilaWinder(filaWinder);
    }
    startAutoConnection() {
        this.autoConnectIntervalID = setInterval(() => this.tryConnectSerialPort(), 1000);
    }
    stopAutoConnection() {
        clearInterval(this.autoConnectIntervalID);
        this.autoConnectIntervalID = null;
    }
    setPortName(port) {
        this.portController.object[this.portController.property] = port.path;
        this.portController.updateDisplay();
    }
    onSerialPortConnectionOpened(port = null) {
        if (port != null) {
            this.setPortName(port);
        }
        this.serialPortConnectionOpened = true;
        this.stopAutoConnection();
        this.interpreter.serialPortConnectionOpened();
        this.folderTitle.find('.serial').addClass('connected');
    }
    onSerialPortConnectionClosed() {
        this.serialPortConnectionOpened = false;
        if (Settings_1.Settings.autoConnect) {
            this.startAutoConnection();
        }
        // this.interpreter.connectionClosed()
        this.folderTitle.find('.serial').removeClass('connected');
    }
    initializePortController(options) {
        this.portController = this.portController.options(options);
        $(this.portController.domElement.parentElement.parentElement).mousedown((event) => {
            this.autoConnectController.setValue(false);
        });
        this.portController.onFinishChange((value) => this.serialConnectionPortChanged(value));
    }
    initializeInterpreter(interpreterName) {
        let filaWinder = this.interpreter ? this.interpreter.filaWinder : null;
        if (this.serialPortConnectionOpened) {
            this.disconnectSerialPort();
        }
        if (interpreterName == 'FilaWinder') {
            this.interpreter = new Interpreter_1.Interpreter(this);
        }
        this.interpreter.setFilaWinder(filaWinder);
        console.log('initialize ' + interpreterName);
    }
    onMessage(event) {
        let json = JSON.parse(event.data);
        let type = json.type;
        let data = json.data;
        if (type == 'opened') {
            this.onSerialPortConnectionOpened();
        }
        else if (type == 'closed') {
            this.onSerialPortConnectionClosed();
        }
        else if (type == 'list') {
            let options = ['Disconnected'];
            for (let port of data) {
                options.push(port.comName);
            }
            this.initializePortController(options);
            if (Settings_1.Settings.autoConnect) {
                for (let port of data) {
                    if (port.manufacturer != null && port.manufacturer.indexOf('Arduino') >= 0) {
                        this.portController.setValue(port.comName);
                        break;
                    }
                }
            }
        }
        else if (type == 'connected') {
            this.setPortName(data);
        }
        else if (type == 'not-connected') {
            this.folderTitle.find('.serial').removeClass('connected').removeClass('simulator');
        }
        else if (type == 'connected-to-simulator') {
            this.folderTitle.find('.serial').removeClass('connected').addClass('simulator');
        }
        else if (type == 'data') {
            this.interpreter.messageReceived(data);
        }
        else if (type == 'warning') {
        }
        else if (type == 'already-opened') {
            this.onSerialPortConnectionOpened(data);
        }
        else if (type == 'error') {
            console.error(data);
        }
    }
    connectToSerial() {
        let firmwareController = this.gui.add(Settings_1.Settings, 'firmware', ['FilaWinder']).name('Firmware');
        firmwareController.onFinishChange((value) => {
            Settings_1.settingsManager.save(false);
            this.initializeInterpreter(value);
        });
        this.autoConnectController = this.gui.add(Settings_1.Settings, 'autoConnect').name('Auto connect').onFinishChange((value) => {
            Settings_1.settingsManager.save(false);
            if (value) {
                this.startAutoConnection();
            }
            else {
                this.stopAutoConnection();
            }
        });
        this.portController = this.gui.add({ 'Connection': 'Disconnected' }, 'Connection');
        this.gui.addButton('Disconnect', () => this.disconnectSerialPort());
        this.gui.addButton('Refresh', () => {
            this.send('list');
        });
        this.initializePortController(['Disconnected']);
        this.socket = new WebSocket('ws://localhost:' + PORT);
        this.socket.addEventListener('message', (event) => this.onMessage(event));
        this.socket.addEventListener('open', (event) => this.onWebSocketOpen(event));
        this.socket.addEventListener('close', (event) => this.onWebSocketClose(event));
        this.socket.addEventListener('error', (event) => this.onWebSocketError(event));
    }
    onWebSocketOpen(event) {
        this.folderTitle.find('.websocket').addClass('connected');
        this.send('is-connected');
    }
    onWebSocketClose(event) {
        this.folderTitle.find('.websocket').removeClass('connected');
        console.error('WebSocket disconnected');
    }
    onWebSocketError(event) {
        console.error('WebSocket error');
        // console.error(event)
    }
    disconnectSerialPort() {
        this.interpreter.clearQueue();
        this.interpreter.sendReset(true);
        this.autoConnectController.setValue(false);
        this.onSerialPortConnectionClosed();
        this.send('close');
        document.dispatchEvent(new CustomEvent('Disconnect'));
        this.portController.setValue('Disconnected');
    }
    serialConnectionPortChanged(portName) {
        if (portName == 'Disconnected' && this.serialPortConnectionOpened) {
            this.disconnectSerialPort();
        }
        else if (portName != 'Disconnected') {
            this.interpreter.setSerialPort(portName);
            document.dispatchEvent(new CustomEvent('Connect', { detail: portName }));
            console.log('open: ' + portName + ', at: ' + this.interpreter.serialCommunicationSpeed);
            this.send('open', { name: portName, baudRate: this.interpreter.serialCommunicationSpeed });
        }
    }
    tryConnectSerialPort() {
        if (!Settings_1.Settings.autoConnect || this.serialPortConnectionOpened) {
            return;
        }
        this.send('list');
    }
    send(type, data = null) {
        let message = { type: type, data: data };
        this.socket.send(JSON.stringify(message));
    }
}
exports.Communication = Communication;
exports.communication = null;


/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
class Controller {
    constructor(controller, gui) {
        this.controller = controller;
        this.gui = gui;
    }
    getDomElement() {
        return this.controller.domElement;
    }
    getParentNames() {
        let names = [];
        let gui = this.gui;
        do {
            names.push(gui.name);
            gui = gui.parent;
        } while (gui != null);
        return names;
    }
    getParentDomElement() {
        return this.getDomElement().parentElement.parentElement;
    }
    contains(element) {
        return this.getParentDomElement().contains(element);
    }
    getProperty() {
        return this.controller.property;
    }
    getName() {
        return this.controller.property;
    }
    getValue() {
        return this.controller.object[this.controller.property];
    }
    onChange(callback) {
        this.controller.onChange(callback);
        return this;
    }
    onFinishChange(callback) {
        this.controller.onFinishChange(callback);
        return this;
    }
    setValue(value, callback = true) {
        if (callback) {
            return this.controller.setValue(value);
        }
        this.setValueNoCallback(value);
    }
    setValueNoCallback(value) {
        this.controller.object[this.controller.property] = value;
        this.controller.updateDisplay();
    }
    max(value, callback = false) {
        this.controller.max(value);
        this.setValue(Math.min(value, this.getValue()), callback);
    }
    min(value, callback = false) {
        this.controller.min(value);
        this.setValue(Math.max(value, this.getValue()), callback);
    }
    step(value) {
        this.controller.step(value);
    }
    updateDisplay() {
        this.controller.updateDisplay();
    }
    options(options) {
        return this.controller.options(options);
    }
    setName(name) {
        this.name(name);
        return this;
    }
    name(name) {
        this.controller.name(name);
        return this;
    }
    hide() {
        $(this.getParentDomElement()).hide();
    }
    show() {
        $(this.getParentDomElement()).show();
    }
}
exports.Controller = Controller;
class GUI {
    constructor(options = null, name = null, parent = null) {
        this.gui = parent != null && name != null ? parent.gui.addFolder(name) : new dat.GUI(options);
        this.name = name;
        this.parent = parent;
        this.nameToController = new Map();
        this.nameToFolder = new Map();
    }
    getDomElement() {
        return this.gui.domElement;
    }
    add(object, propertyName, min = null, max = null, step = null) {
        let controller = new Controller(this.gui.add(object, propertyName, min, max, step), this);
        this.nameToController.set(propertyName, controller);
        return controller;
    }
    addButton(name, callback) {
        let object = {};
        object[name] = callback;
        return this.add(object, name);
    }
    setName(name) {
        $(this.getDomElement()).find('li.title').text(name);
    }
    addFileSelectorButton(name, fileType, callback) {
        let divJ = $("<input data-name='file-selector' type='file' class='form-control' name='file[]'  accept='" + fileType + "'/>");
        let button = this.addButton(name, (event) => divJ.click());
        // $(button.getDomElement()).append(divJ)
        divJ.insertAfter(button.getParentDomElement());
        divJ.hide();
        divJ.change((event) => {
            callback(event);
            divJ.val('');
        });
        return button;
    }
    addSlider(name, value, min, max, step = null) {
        let object = {};
        object[name] = value;
        let slider = this.add(object, name, min, max);
        if (step != null) {
            slider.step(step);
        }
        return slider;
    }
    addFolder(name) {
        let folder = new GUI(null, name, this);
        this.nameToFolder.set(name, folder);
        return folder;
    }
    getController(name) {
        return this.nameToController.get(name);
    }
    getControllers() {
        let keyValues = Array.from(this.nameToController);
        return Array.from(keyValues, keyValue => keyValue[1]);
    }
    getAllControllers() {
        let controllers = this.getControllers();
        for (let f of this.nameToFolder) {
            let folder = f[1];
            controllers = controllers.concat(folder.getAllControllers());
        }
        return controllers;
    }
    getFolder(name) {
        return this.nameToFolder.get(name);
    }
    getFolders() {
        let keyValues = Array.from(this.nameToFolder);
        return Array.from(keyValues, keyValue => keyValue[1]);
    }
    hide() {
        $(this.gui.domElement).hide();
    }
    show() {
        $(this.gui.domElement).show();
    }
    open() {
        this.gui.open();
    }
    close() {
        this.gui.close();
    }
}
exports.GUI = GUI;


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const Communication_1 = __webpack_require__(1);
class CommandDisplay {
    constructor() {
        // $('#commands-content').click((event)=> this.click(event))
        document.addEventListener('QueueCommand', (event) => this.queueCommand(event.detail), false);
        document.addEventListener('SendCommand', (event) => this.sendCommand(event.detail), false);
        document.addEventListener('CommandExecuted', (event) => this.commandExecuted(event.detail), false);
        document.addEventListener('ClearQueue', (event) => this.clearQueue(), false);
    }
    createGUI(gui) {
        this.gui = gui.addFolder('Commands');
        this.gui.open();
        this.pauseButton = this.gui.add({ 'Pause': false }, 'Pause').onChange((value) => Communication_1.communication.interpreter.setPause(value));
        this.gui.addButton('Emergency stop', () => {
            this.pauseButton.setValue(true);
            Communication_1.communication.interpreter.sendReset(true);
        });
        this.gui.addButton('Clear commands', () => Communication_1.communication.interpreter.clearQueue());
        let commandList = this.gui.addFolder('Command list');
        this.listJ = $('<ul id="command-list" class="c-list">');
        commandList.open();
        this.listJ.insertAfter($(commandList.gui.domElement).find('li'));
    }
    click(event) {
        if (event.target.tagName == 'BUTTON') {
            let commandID = parseInt(event.target.parentNode.id);
            Communication_1.communication.interpreter.removeCommand(commandID);
            this.removeCommand(commandID);
        }
    }
    createCommandItem(command) {
        let liJ = $('<li id="' + command.id + '"">');
        let messageJ = $('<div>').append(command.message).addClass('message');
        let dataJ = $('<div>').append(command.data).addClass('data');
        liJ.append(messageJ);
        liJ.append(dataJ);
        let closeButtonJ = $('<button>x</button>');
        closeButtonJ.click((event) => this.removeCommand(command.id));
        liJ.append(closeButtonJ);
        return liJ;
    }
    removeCommand(id) {
        this.listJ.find('#' + id).remove();
        this.updateName();
        document.dispatchEvent(new CustomEvent('CommandListChanged'));
    }
    updateName() {
        $('#commands h3').text('Command list (' + this.listJ.children().length + ')');
    }
    queueCommand(command) {
        this.listJ.append(this.createCommandItem(command));
        this.updateName();
        document.dispatchEvent(new CustomEvent('CommandListChanged'));
    }
    sendCommand(command) {
        this.listJ.find('#' + command.id).addClass('sent');
    }
    commandExecuted(command) {
        this.removeCommand(command.id);
    }
    clearQueue() {
        this.listJ.children().remove();
        this.updateName();
        document.dispatchEvent(new CustomEvent('CommandListChanged'));
    }
}
exports.CommandDisplay = CommandDisplay;


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const GUI_1 = __webpack_require__(2);
class Console {
    constructor() {
        this.MAX_NUM_MESSAGES = 1000;
        this.scrollingToBottom = false;
        this.skipScrollToBottom = false;
        document.addEventListener('CommandListChanged', (event) => this.scrollToBottom(), false);
        this.log = console.log.bind(console);
        this.error = console.error.bind(console);
        this.info = console.info.bind(console);
        this.table = console.table.bind(console);
        let log = (args, logger, type) => {
            if (typeof logger === 'function') {
                logger.apply(console, args);
            }
            let div = $('<li>');
            if (type == 'table') {
                let p = this.logTable.apply(this, args);
                div.append(p);
            }
            else {
                for (let arg of args) {
                    let p = null;
                    if (typeof arg == 'object') {
                        p = this.logObject(arg);
                    }
                    else if (arg instanceof Array) {
                        let result = JSON.stringify(arg);
                        if (result.length > 100) {
                            result = result.substr(0, 20) + '...' + result.substr(result.length - 20);
                        }
                        p = $('<p>').append(result).addClass(type);
                    }
                    else {
                        p = $('<p>').append(arg).addClass(type);
                    }
                    div.append(p);
                }
            }
            let consoleJ = this.listJ;
            if (consoleJ.children().length >= this.MAX_NUM_MESSAGES) {
                consoleJ.find('li:first-child').remove();
            }
            consoleJ.append(div);
            this.scrollToBottom(consoleJ);
        };
        console.log = (...args) => log(args, this.log, 'log');
        console.error = (...args) => log(args, this.error, 'error');
        console.info = (...args) => log(args, this.info, 'info');
        console.table = (...args) => log(args, this.table, 'table');
        this.gui = new GUI_1.GUI({ autoPlace: false });
        let customContainer = document.getElementById('info');
        customContainer.appendChild(this.gui.getDomElement());
    }
    createGUI() {
        this.folder = this.gui.addFolder('Console');
        this.folder.open();
        this.listJ = $('<ul id="console-list" class="c-list">');
        this.listJ.insertAfter($(this.folder.gui.domElement).find('li'));
        this.listJ.scroll((event) => {
            if (!this.scrollingToBottom) {
                let consoleE = this.listJ.get(0);
                this.skipScrollToBottom = consoleE.scrollTop + consoleE.clientHeight < consoleE.scrollHeight;
            }
            this.scrollingToBottom = false;
        });
        this.updateMaxHeight();
        window.addEventListener('resize', () => this.updateMaxHeight(), false);
        $('#info').click(() => this.updateMaxHeight()); // to handle the case when user opens / closes a folder
    }
    updateMaxHeight() {
        this.listJ.css('max-height', $('#info').outerHeight() - this.listJ.offset().top);
    }
    scrollToBottom(consoleJ = this.listJ) {
        this.updateMaxHeight();
        if (this.skipScrollToBottom) {
            return;
        }
        this.scrollingToBottom = true;
        consoleJ.scrollTop(consoleJ.get(0).scrollHeight);
    }
    printTable(objArr, keys) {
        var numCols = keys.length;
        var len = objArr.length;
        var $table = document.createElement('table');
        $table.style.width = '100%';
        $table.setAttribute('border', '1');
        var $head = document.createElement('thead');
        var $tdata = document.createElement('td');
        $tdata.innerHTML = 'Index';
        $head.appendChild($tdata);
        for (var k = 0; k < numCols; k++) {
            $tdata = document.createElement('td');
            $tdata.innerHTML = keys[k];
            $head.appendChild($tdata);
        }
        $table.appendChild($head);
        for (var i = 0; i < len; i++) {
            var $line = document.createElement('tr');
            let $tdata = document.createElement('td');
            $tdata.innerHTML = i;
            $line.appendChild($tdata);
            for (var j = 0; j < numCols; j++) {
                $tdata = document.createElement('td');
                $tdata.innerHTML = objArr[i][keys[j]];
                $line.appendChild($tdata);
            }
            $table.appendChild($line);
        }
        return $table;
    }
    logObject(object) {
        let properties = [];
        for (let property in object) {
            properties.push({ name: property, value: object[property] });
        }
        return this.printTable(properties, ['name', 'value']);
    }
    ;
    logTable(...args) {
        var objArr = args[0];
        var keys;
        if (typeof objArr !== 'undefined') {
            keys = Object.keys(objArr);
        }
        return this.printTable(objArr, keys);
    }
}
exports.Console = Console;


/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const Communication_1 = __webpack_require__(1);
const Settings_1 = __webpack_require__(0);
class FilaWinder {
    constructor() {
        this.gui = null;
        this.initializedCommunication = false;
        this.previousSpeed = 0;
    }
    checkInitialized() {
        if (!this.initializedCommunication) {
            Communication_1.communication.interpreter.initialize();
        }
    }
    createGUI(gui) {
        this.gui = gui;
        // let position = { moveX: Settings.filaWinder.homeX, moveY: Settings.filaWinder.homeY }
        // gui.add(position, 'moveX', 0, Settings.filaWinder.width).name('Move X').onFinishChange((value)=> this.setX(value))
        // gui.add(position, 'moveY', 0, Settings.filaWinder.height).name('Move Y').onFinishChange((value)=> this.setY(value))
        gui.addButton('Calibrate', () => this.calibrate());
        gui.addButton('Start', () => this.start());
        gui.addButton('Stop', () => this.stop());
        gui.addButton('Constant speed', () => this.constantSpeed());
        // this.penStateButton = gui.addButton('Pen down', () => this.togglePenState() )
        // this.motorsEnableButton = gui.addButton('Disable motors', ()=> this.toggleMotors())
        // // DEBUG
        // gui.addButton('Send specs', ()=> communication.interpreter.initialize(false))
    }
    initialize() {
        let background = paper.Path.Rectangle(paper.project.view.bounds);
        background.fillColor = 'white';
        background.strokeColor = null;
        this.guide = new paper.Group();
        this.guideLeft = new paper.Path();
        this.guideRight = new paper.Path();
        this.guideCurrent = new paper.Path();
        this.guideLeft.applyMatrix = false;
        this.guideRight.applyMatrix = false;
        this.guideCurrent.applyMatrix = false;
        this.guideLeft.strokeWidth = 1;
        this.guideRight.strokeWidth = 1;
        let pivot = new paper.Point(0, 0);
        this.guideLeft.add(new paper.Point(0, 0));
        this.guideLeft.add(new paper.Point(200, 0));
        this.guideLeft.pivot = pivot;
        this.guideLeft.rotation = 0;
        this.guide.addChild(this.guideLeft);
        this.guideRight.add(new paper.Point(0, 0));
        this.guideRight.add(new paper.Point(200, 0));
        this.guideRight.pivot = pivot;
        this.guideRight.rotation = 0;
        this.guide.addChild(this.guideRight);
        this.guideCurrent.add(new paper.Point(0, 0));
        this.guideCurrent.add(new paper.Point(200, 0));
        this.guideCurrent.strokeColor = 'blue';
        this.guideCurrent.strokeWidth = 2;
        this.guideCurrent.pivot = pivot;
        this.guideCurrent.rotation = 0;
        this.guide.addChild(this.guideCurrent);
        paper.settings.hitTolerance = 20;
        paper.project.activeLayer.onMouseMove = (event) => {
            let hitResult = paper.project.hitTest(event.point);
            if (hitResult.item.parent == this.guide) {
                $('#canvas').addClass('pointer');
            }
            else {
                $('#canvas').removeClass('pointer');
            }
        };
        // paper.project.activeLayer.onMouseUp = (event: any)=> {
        // 	let hitResult = paper.project.hitTest(event.point)
        // 	if(hitResult.item == this.guideLeft) {
        // 		$('#canvas').addClass('pointer')
        // 	} else {
        // 		$('#canvas').removeClass('pointer')
        // 	}
        // }
        this.guide.position = paper.project.view.bounds.center.add(this.guide.bounds.size.multiply(0.5));
        this.servoLeftChanged(false);
        this.servoRightChanged(false);
        this.servoCurrentChanged(false);
    }
    start() {
        Communication_1.communication.interpreter.sendStart();
    }
    stop() {
        Communication_1.communication.interpreter.sendReset();
    }
    calibrate() {
        Communication_1.communication.interpreter.sendCalibrate();
    }
    constantSpeed() {
        Communication_1.communication.interpreter.sendReset();
        Communication_1.communication.interpreter.sendConstantSpeed();
    }
    servoLeftChanged(sendChange) {
        if (sendChange) {
            Communication_1.communication.interpreter.sendServoLeft();
        }
        this.guideLeft.rotation = -180 + Settings_1.Settings.guide.left;
    }
    servoRightChanged(sendChange) {
        if (sendChange) {
            Communication_1.communication.interpreter.sendServoRight();
        }
        this.guideRight.rotation = -180 + Settings_1.Settings.guide.right;
    }
    servoCurrentChanged(sendChange) {
        if (sendChange) {
            Communication_1.communication.interpreter.sendServoCurrent();
        }
        this.guideCurrent.rotation = -180 + Settings_1.Settings.guide.current;
    }
    speedChanged(sendChange) {
        if (sendChange && this.previousSpeed != Settings_1.Settings.speed) {
            Communication_1.communication.interpreter.sendReset();
            Communication_1.communication.interpreter.sendSpeed();
            Communication_1.communication.interpreter.sendConstantSpeed();
            this.previousSpeed = Settings_1.Settings.speed;
        }
    }
    keyDown(event) {
        let amount = event.shiftKey ? 25 : event.ctrlKey ? 10 : event.altKey ? 5 : 1;
        switch (event.keyCode) {
            case 37:
                // this.moveDirect(this.getPosition().add(new paper.Point(-amount, 0)))
                break;
            case 38:
                // this.moveDirect(this.getPosition().add(new paper.Point(0, -amount)))
                break;
            case 39:
                // this.moveDirect(this.getPosition().add(new paper.Point(amount, 0)))
                break;
            case 40:
                // this.moveDirect(this.getPosition().add(new paper.Point(0, amount)))
                break;
            default:
                break;
        }
    }
    keyUp(event) {
    }
    windowResize() {
    }
}
exports.FilaWinder = FilaWinder;
exports.filaWinder = new FilaWinder();


/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
class Renderer {
    constructor() {
        this.canvas = document.createElement('canvas');
        let containerJ = $('#canvas');
        this.canvas.width = containerJ.width();
        this.canvas.height = containerJ.height();
        containerJ.get(0).appendChild(this.canvas);
        paper.setup(this.canvas);
        paper.project.currentStyle.strokeColor = 'black';
        paper.project.currentStyle.strokeWidth = 0.5;
        paper.project.currentStyle.strokeScaling = false;
        this.dragging = false;
        this.previousPosition = new paper.Point(0, 0);
        document.addEventListener('SettingChanged', (event) => this.onSettingChanged(event), false);
    }
    onSettingChanged(event) {
        // if(event.detail.all || event.detail.parentNames[0] == 'Machine dimensions') {
        // this.centerOnFilaWinder(Settings.filaWinder, true)
        // }
    }
    centerOnFilaWinder(filaWinder, zoom = true) {
        if (zoom) {
            let margin = 200;
            let ratio = Math.max((filaWinder.width + margin) / this.canvas.width * window.devicePixelRatio, (filaWinder.height + margin) / this.canvas.height * window.devicePixelRatio);
            paper.view.zoom = 1 / ratio;
        }
        paper.view.setCenter(new paper.Point(filaWinder.width / 2, filaWinder.height / 2));
    }
    getDomElement() {
        return paper.view.element;
    }
    windowResize() {
        let containerJ = $('#canvas');
        let width = containerJ.width();
        let height = containerJ.height();
        let canvasJ = $(this.canvas);
        canvasJ.width(width);
        canvasJ.height(height);
        paper.view.viewSize = new paper.Size(width, height);
    }
    getMousePosition(event) {
        return new paper.Point(event.clientX, event.clientY);
    }
    getWorldPosition(event) {
        return paper.view.viewToProject(this.getMousePosition(event));
    }
    mouseDown(event) {
        this.dragging = true;
        this.previousPosition = this.getMousePosition(event);
    }
    mouseMove(event) {
        if (event.buttons == 4 || this.spacePressed && this.dragging) {
            let position = this.getMousePosition(event);
            paper.view.translate(position.subtract(this.previousPosition).divide(paper.view.zoom));
            paper.view.draw();
            this.previousPosition.x = position.x;
            this.previousPosition.y = position.y;
        }
    }
    mouseUp(event) {
        this.dragging = false;
    }
    mouseLeave(event) {
        this.dragging = false;
    }
    mouseWheel(event) {
        if (event.target != this.getDomElement()) {
            return;
        }
        let cursorPosition = this.getWorldPosition(event);
        paper.view.zoom = Math.max(0.1, Math.min(5, paper.view.zoom + event.deltaY / 500));
        let newCursorPosition = this.getWorldPosition(event);
        paper.view.translate(newCursorPosition.subtract(cursorPosition));
    }
    keyDown(event) {
        switch (event.keyCode) {
            case 32:
                this.spacePressed = true;
                $('#canvas').addClass('grab');
        }
    }
    keyUp(event) {
        switch (event.keyCode) {
            case 32:
                this.spacePressed = false;
                $('#canvas').removeClass('grab');
        }
    }
    render() {
    }
}
exports.Renderer = Renderer;


/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const Settings_1 = __webpack_require__(0);
const MAX_INPUT_BUFFER_LENGTH = 500;
class Interpreter {
    constructor(communication) {
        this.commandID = 0;
        this.continueMessage = 'READY';
        this.serialCommunicationSpeed = 115200;
        this.commandQueue = [];
        this.pause = false;
        this.serialInput = '';
        this.tempoNextCommand = false;
        this.communication = communication;
    }
    setSerialPort(serialPort) {
        this.serialPort = serialPort;
    }
    setFilaWinder(filaWinder) {
        this.filaWinder = filaWinder;
    }
    serialPortConnectionOpened() {
        this.initialize();
    }
    initialize(initializeAtHome = true) {
        this.filaWinder.initializedCommunication = true;
    }
    send(command) {
        if (this.pause) {
            return;
        }
        document.dispatchEvent(new CustomEvent('SendCommand', { detail: command }));
        console.log('send: ' + command.message + ' - ' + command.data);
        this.communication.send('data', command.data);
    }
    messageReceived(message) {
        if (message == null) {
            return;
        }
        this.serialInput += message;
        let messages = this.serialInput.split('\n');
        // process all messages except the last one (it is either empty if the serial input ends with '\n', or it is not a finished message)
        for (let i = 0; i < messages.length - 1; i++) {
            this.processMessage(messages[i]);
        }
        // Clear any old message
        if (this.serialInput.endsWith('\n')) {
            this.serialInput = '';
        }
        else {
            this.serialInput = messages[messages.length - 1];
        }
    }
    processMessage(message) {
        if (message.indexOf('-p: l: ') != 0) {
            console.log(message);
        }
        // if(message.indexOf('++')==0) {
        // 	console.log(message)
        // }
        document.dispatchEvent(new CustomEvent('MessageReceived', { detail: message }));
        if (message.indexOf(this.continueMessage) == 0) {
            if (this.commandQueue.length > 0) {
                let command = this.commandQueue.shift();
                if (command.callback != null) {
                    command.callback();
                }
                document.dispatchEvent(new CustomEvent('CommandExecuted', { detail: command }));
                if (this.commandQueue.length > 0) {
                    this.send(this.commandQueue[0]);
                }
                else {
                    this.queueEmpty();
                }
            }
        }
    }
    queueEmpty() {
    }
    setPause(pause) {
        this.pause = pause;
        if (!this.pause && this.commandQueue.length > 0) {
            this.send(this.commandQueue[0]);
        }
    }
    queue(data, message, callback = null) {
        let command = { id: this.commandID++, data: data, callback: callback, message: message };
        document.dispatchEvent(new CustomEvent('QueueCommand', { detail: command }));
        this.commandQueue.push(command);
        if (this.commandQueue.length == 1) {
            this.send(command);
        }
    }
    removeCommand(commandID) {
        let index = this.commandQueue.findIndex((command) => command.id == commandID);
        if (index >= 0) {
            this.commandQueue.splice(index, 1);
        }
    }
    clearQueue() {
        this.commandQueue = [];
        document.dispatchEvent(new CustomEvent('ClearQueue', { detail: null }));
    }
    sendStart(callback = null) {
        let message = 'Start';
        this.queue('G0\n', message, callback);
    }
    sendStop(callback = null) {
        let message = 'Stop';
        this.queue('G1\n', message, callback);
    }
    sendCalibrate(callback = null) {
        let message = 'Calibrate';
        this.queue('M4\n', message, callback);
    }
    sendConstantSpeed(callback = null) {
        let message = 'Constant speed';
        this.queue('M5\n', message, callback);
    }
    sendSpeed(speed = Settings_1.Settings.speed, callback = null) {
        let message = 'Set speed: ' + speed.toFixed(2);
        this.queue('M6 F' + speed.toFixed(2) + '\n', message, callback);
    }
    sendSetup(callback = null) {
        let message = 'Setup';
        this.queue('M10\n', message, callback);
    }
    sendReset(force = true) {
        if (force) {
            this.communication.send('data', 'M0\n');
            return;
        }
        let message = 'Stop';
        this.queue('M0\n', message);
    }
    sendServoLeft(value = Settings_1.Settings.guide.left, callback = null) {
        let message = 'Set servo left: ' + value.toFixed(2);
        this.queue('M1 X' + value.toFixed(2) + '\n', message, callback);
    }
    sendServoRight(value = Settings_1.Settings.guide.right, callback = null) {
        let message = 'Set servo right: ' + value.toFixed(2);
        this.queue('M2 X' + value.toFixed(2) + '\n', message, callback);
    }
    sendServoCurrent(value = Settings_1.Settings.guide.current, callback = null) {
        let message = 'Set servo current: ' + value.toFixed(2);
        this.queue('M3 X' + value.toFixed(2) + '\n', message, callback);
    }
}
exports.Interpreter = Interpreter;


/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/// <reference path="../node_modules/@types/three/index.d.ts"/>
/// <reference path="../node_modules/@types/jquery/index.d.ts"/>
/// <reference path="../node_modules/@types/paper/index.d.ts"/>
/// <reference path="../node_modules/@types/file-saver/index.d.ts"/>
Object.defineProperty(exports, "__esModule", { value: true });
// import Stats = require("../node_modules/three/examples/js/libs/stats.min.js")
// import { Stats } from "../node_modules/three/examples/js/libs/stats.min.js"
// import { THREE } from "../node_modules/three/build/three"
const Settings_1 = __webpack_require__(0);
// import { FilaWinder, filaWinder } from "./FilaWinder"
const Renderer_1 = __webpack_require__(6);
// import { Pen } from "./Pen"
// import { SVGPlot } from "./Plot"
const Communication_1 = __webpack_require__(1);
const CommandDisplay_1 = __webpack_require__(3);
const GUI_1 = __webpack_require__(2);
const Console_1 = __webpack_require__(4);
const FilaWinder_1 = __webpack_require__(5);
let communication = null;
let container = null;
let renderer = null;
let gui;
let positionPreview = null;
let drawing = {
    scale: 1,
};
let w = window;
document.addEventListener("DOMContentLoaded", function (event) {
    function initialize() {
        dat.GUI.DEFAULT_WIDTH = 325;
        gui = new GUI_1.GUI({ autoPlace: false });
        let controllerConsole = new Console_1.Console();
        let commandDisplay = new CommandDisplay_1.CommandDisplay();
        commandDisplay.createGUI(controllerConsole.gui);
        controllerConsole.createGUI();
        let customContainer = document.getElementById('gui');
        customContainer.appendChild(gui.getDomElement());
        communication = new Communication_1.Communication(gui);
        communication.setFilaWinder(FilaWinder_1.filaWinder);
        let commandFolder = gui.addFolder('Commands');
        commandFolder.open();
        renderer = new Renderer_1.Renderer();
        FilaWinder_1.filaWinder.createGUI(commandFolder);
        FilaWinder_1.filaWinder.initialize();
        Settings_1.settingsManager.createGUI(gui);
        Settings_1.settingsManager.setFilaWinder(FilaWinder_1.filaWinder);
        // debug
        w.settingsManager = Settings_1.settingsManager;
        w.gui = gui;
        w.renderer = renderer;
        w.communication = communication;
        w.commandDisplay = commandDisplay;
    }
    initialize();
    let animate = () => {
        w.nCall = 0;
        requestAnimationFrame(animate);
        renderer.render();
    };
    animate();
    function windowResize() {
        renderer.windowResize();
    }
    function eventWasOnGUI(event) {
        return $.contains(gui.getDomElement(), event.target);
    }
    function mouseDown(event) {
        renderer.mouseDown(event);
    }
    function mouseMove(event) {
        renderer.mouseMove(event);
    }
    function mouseUp(event) {
        renderer.mouseUp(event);
    }
    function mouseLeave(event) {
        renderer.mouseLeave(event);
    }
    function mouseWheel(event) {
        renderer.mouseWheel(event);
    }
    function keyDown(event) {
        FilaWinder_1.filaWinder.keyDown(event);
        renderer.keyDown(event);
    }
    function keyUp(event) {
        FilaWinder_1.filaWinder.keyUp(event);
        renderer.keyUp(event);
    }
    window.addEventListener('resize', windowResize, false);
    document.body.addEventListener('mousedown', mouseDown);
    document.body.addEventListener('mousemove', mouseMove);
    document.body.addEventListener('mouseup', mouseUp);
    document.body.addEventListener('mouseleave', mouseLeave);
    document.body.addEventListener('keydown', keyDown);
    document.body.addEventListener('keyup', keyUp);
    addWheelListener(document.body, mouseWheel);
});


/***/ })
/******/ ]);